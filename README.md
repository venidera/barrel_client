# Barrel Client - An HTTP Python Client to [VDSF Barrel](https://bitbucket.org/venidera/barrel)

![GitHub Release](https://img.shields.io/badge/release-v0.0.32-blue.svg)
![GitHub license](https://img.shields.io/badge/license-GPLv3-yellow.svg)

The Barrel Client is a Python package that handles HTTP calls with the [VDSF Barrel Server](https://bitbucket.org/venidera/barrel).

## Table of contents

[TOC]

## Requirements

* Python version 3.6 or superior, and the libraries:
    - `python3.6-venv`
    - `python3.6-dev`

### Windows

You'll need DLLs for libmagic found in: https://pypi.python.org/pypi/python-magic-bin/0.4.14. Other sources of the libraries in the past have been [File for Windows](http://gnuwin32.sourceforge.net/packages/file.htm) .  You will need to copy the file `magic` out of `[binary-zip]\share\misc`, and pass its location to `Magic(magic_file=...)`.  

If you are using a 64-bit build of python, you'll need 64-bit libmagic binaries which can be found here: https://github.com/pidydx/libmagicwin64. Newer version can be found here: https://github.com/nscaife/file-windows.

### OSX

- When using Homebrew: `brew install libmagic`
- When using macports: `port install file`


## Installation

### Using PIP

We present here an installation with a **virtualenv** using Python 3.6:

```bash
$ /usr/bin/python3.6 -m venv venv-3.6 --prompt="Barrel Client"
$ source venv-3.6/bin/activate; 
(Barrel Client) $ pip install --upgrade pip setuptools
(Barrel Client) $ pip install git+https://git@bitbucket.org/venidera/barrel_client.git
(Barrel Client) $ pip list
Package       Version   
------------- ----------
barrel-client 0.0.32  
certifi       2018.10.15
chardet       3.0.4     
idna          2.7       
pip           18.1      
pkg-resources 0.0.0 
pyOpenSSL     18.0.0    
python-magic  0.4.15    
requests      2.20.1    
setuptools    40.5.0    
urllib3       1.24.1 
```

### For Development

In order to install the Barrel Client application, please type:

```bash
$ git clone git@bitbucket.org:venidera/barrel_client.git
```



## Development and tests
To avoid errors, the development can be done in the repo directory but the tests must be executed in an isolated directory because `__init__.py` files can hide import errors. An example of installation for tests is presented below:

```
$ cd <repo>
$ mkdir tests
$ cd tests
$ /usr/bin/python3.6 -m venv venv-3.6 --prompt="Barrel Client"
$ source venv-3.6/bin/activate
(Barrel Client) $ pip install pip setuptools --upgrade
(Barrel Client) $ cd .. ; python setup.py install ; cd -
```

The steps listed above will create a virtualenv with the package installed. If you change the code of the package you must execute:

```
$ cd .. ; python setup.py install ; cd -
```

The tests must consider the import call of the package and its classes and functions must be tested. If everything works fine, commit changes and execute `git pull && git push`. All files created inside the `tests` directory must be kept untracked in the repository.

## Documentation

The Barrel API documentation can be accessed [here](https://www.getpostman.com/collections/969817aa198bc2c3408d).
The package directory tree is described as follows:

```
barrel_client/
├── barrel_client/
   └── client.py
```

The **client.py** file have all [VDSF Barrel](https://bitbucket.org/venidera/barrel) stable endpoints. There are **two options for returning a result** when using Barrel Client, where each one is defined by assigning the values `True` or `False` to a variable called `httpresp`. If `httpresp=True`, then the endpoint will return exactly the same response that is presented in the Barrel documentation, plus a `status_code`. Otherwise, it will return a parsed content, just considering the data of the response. Take a look at the list of implemented endpoints presented below, when considering the following portion of code (which instantiate the client and the models) as a common part for all of them and `httpresp=False`.

```python
import logging
from barrel_client import Connection

# General procedures to show the log
logging.basicConfig(
    format='%(asctime)s - %(levelname)s: ' +
           '(%(filename)s:%(funcName)s at %(lineno)d): %(message)s',
    datefmt='%b %d %H:%M:%S',
    level=logging.INFO)
# Creating connection instance
c = Connection('miran-barrel.venidera.net', 9090)
```

### Authentication

Authentication in Miran platform will require access to the Auth-Center, on the VPC it is deployed at *auth-center.venidera.cloud:10443*. In order to run the application in a local environment, one can execute the following command: `ssh -f prod -L 9090:192.168.40.10:9090 -N`. Obs: make sure you have the right privileges to access the current server. Take a look at the example below:

```python
# Checking if the user is logged at the API
>>> c.is_logged()
False
# Logging at the Miran API with the wrong password
>>> c.api_login('rafael@venidera.com','##wrong_password##')
False
# Logging at the Miran API with the right password
>>> c.api_login('rafael@venidera.com','##correct_password##')
True
# Checking again if the user is logged at the API
>>> c.is_logged()
True
# Capturing API version info
>>> c.get_version()
{'status': 'success', 'message': 'Barrel API version v.2.0 - 2019-09-20', 'status_code': 200}
# Logging out at the API
>>> c.api_logout()
# Checking again if the user is logged at the API
>>> c.is_logged()
False
>>> c.get_version()
Traceback (most recent call last):
  File "tests/connection.py", line 19, in <module>
    print(c.get_version())
  File "/home/rafael/git/src/barrel_client/tests/venv/lib/python3.6/site-packages/barrel_client-0.0.32-py3.6.egg/barrel_client/client.py", line 431, in get_version
  File "/home/rafael/git/src/barrel_client/tests/venv/lib/python3.6/site-packages/barrel_client-0.0.32-py3.6.egg/barrel_client/client.py", line 238, in get
AssertionError: You must login to use barrel client.
```

### Permissioning

Acessing to Miran objects requires previous authentication using the `api_login()` method with proper credentials. The permissioning of such objects (i.e., permissions for creating, viewing, modifying and deleting objects) are defined according to the organization in which the logged user is registered. For example, the users `rafael@venidera.com` and `marcos@venidera.com` belong to the organization called `Venidera` and, therefore, they share the same object permission rules. On the other hand, the user `paulo@aneel.org.br` belongs to the `Aneel` organization and has different permission rules in relation to `Venidera` users.

When an object is inserted by a logged user, the object\`s read and write permissions are granted for the user\`s organization only. This means that, if the user `rafael@venidera.com` inserts an entity in the database, the user `paulo@aneel.org.br` cannot see or modify such object, but the users `marcos@venidera.com` and `lucas@venidera.com` can. Organizations are defined by their *nicknames*. A list of existing organizations can be found by running the code below:

```python
# Logging in with a Venidera user
>>> c.api_login('rafael@venidera.com','##correct_password##')
# Capturing existing organizations
>>> c.get_organizations()
[{'name': 'Venidera Pesquisa e Desenvolvimento', 'nickname': 'Venidera'}, {'name': 'Elektro Eletricidade e Serviços S.A.', 'nickname': 'Elektro'}, {'name': 'Aratu Geração S.A.', 'nickname': 'Aratu'}, {'name': 'Mineração Santa Elina Indústria e Comércio S.A.', 'nickname': 'Santa Elina'}, {'name': 'Agência Nacional de Energia Elétrica (ANEEL)', 'nickname': 'ANEEL'}, {'name': 'EDP Energias do Brasil S.A.', 'nickname': 'EDP'}, {'name': 'MATRIX SERVICES CONSULTORIA E GESTÃO EM ENERGIA LTDA', 'nickname': 'Matrix'}, {'name': 'PRIME ENERGY COMERCIALIZADORA DE ENERGIA LTDA', 'nickname': 'Prime'}, {'name': 'INSTITUTO ACENDE BRASIL', 'nickname': 'ACENDE BRASIL'}, {'name': 'Policonsult', 'nickname': 'POLICONSULT'}, {'name': 'CORTES ASSESSORIA E CONSULTORIA LTDA', 'nickname': 'CORTES'}, {'name': 'SINERCONSULT CONSULTORIA TREINAMENTO E PARTICIPAÇÕES LTDA,', 'nickname': 'SINERCONSULT'}, {'name': 'Venidera - Colaboradores', 'nickname': 'VenideraCo'}, {'name': 'AES TIETÊ ENERGIA S.A.', 'nickname': 'AES TIETÊ'}, {'name': 'OPERADOR NACIONAL DO SISTEMA ELETRICO', 'nickname': 'ONS'}, {'name': 'CAMARA DE COMERCIALIZACAO DE ENERGIA ELETRICA - CCEE', 'nickname': 'CCEE'}]
```

Objects are created as private by default (only users organization can access or modify them). However, objects can be set as public through a change in their permission rules, and vice versa. Also, objects can be created as public (only for reading). The following pieces of code present the some examples to set custom permissions when inserting a new entity object:

```python
# Logging in with a Venidera user
>>> c.api_login('rafael@venidera.com','##correct_password##')
# Adding an entity with custom permissions (example 1)
>>> c.add_entity(data={
        "ids": {"entity": "entity_teste_2020"},
        "data": {
            "title": "Title",
            "description": "Description"
        },
        "metadata": {},
        "location": [-20.799879, -42.117518],
        "type": "Entity_teste",
        # Dictionary with read (GET) and write (PUT, DELETE) permissions 
        "permissions": {
            # Users from Aneel and Elektro can read the object
            "read": ["Aneel", "Elektro"],
            # Users from Aneel can update or delete the object
            "write": ["Aneel"]
        }
    })
# Adding an entity with custom permissions (example 2)
>>> c.add_entity(data={
        ...
        "permissions": {
            # Users from any organization can read the object
            "read": [],
            # Users from Aneel and Elektro can update or delete the object
            "write": ["Aneel", "Elektro"]
        }
    })
```

It is worth mentionating that **objects cannot have their write permissions set to public**. Also, permission rules are applied to derivations of objects (such as files and time series).

## Client Methods

The following methods describe each functionality of Miran Barrel.

### `get_version()`
This endpoint shows the current version of the Barrel Client in the log. There are no arguments to be passed.
```python
>>> c.get_version()
{'status': 'success', 'message': 'Barrel API version v.2.0 - 2019-09-20', 'status_code': 200}
```

### `get_types(otype=str(), params=dict())`
This endpoint returns a `list` with the type and the amount all objects in the database. There is two optional arguments: one called `otype`, which represents the type of the required object (in the case of returning just the amount of one specific type) and another called `params`, which acts as a filter for the required search.

```python
>>> c.get_types()
['Empresa', 'Entity', 'Executora', 'Relationship', 'Casa']
```

### `get_entity_types(params=dict())`
This endpoint returns a `list` of all types of Entity objects in the database, or `False` in the case of no Entity types. There is one optional argument (`params`) which acts as a filter for the required search.
```python
>>> c.get_entity_types(params={"limit": 2})
['Empresa', 'Casa']
```

### `get_relationship_types(params=dict())`
This endpoint returns a `list` of all types of Relationship objects in the database, or `False` in the case of no Relationship types. There is one optional argument (`params`) which acts as a filter for the required search.
```python
>>> c.get_relationship_types()
['Executora']
```

### `get_entity(oid=str(), params=dict())`
This endpoint performs several types of searches on Entity objects. There are two required arguments to be informed. The first one (`oid`) is an `str` which represents the **Entity id** field, usually in the following format: `entX_Y`, where `X` and `Y` are two integer numbers. The second argument (`params`) is a `dict` which acts as a filter for the required search. The following code presents three examples of endpoint usage:
```python
# First example - Getting all entities
>>> c.get_entity()
[{'ts': [], 'location': [-22.799879, -47.117518], 'metadata': {'added': '2016-19-06T07:47:12.496944'}, 'data': {'contact': {'email': 'rafael@venidera.com'}}, 'ids': {'cpf': 866313}, 'id': 'ent13_1', 'type': 'Casa'}, {'ts': [], 'location': [-20.799879, -42.117518], 'metadata': {'added': '2016-19-06T07:47:12.496944'}, 'data': {'contact': {'email': 'user@server.com'}, 'name': 'The Name of the Entity represented in this Object'}, 'ids': {'some_id': 'some_value'}, 'id': 'ent15_0', 'type': 'Empresa'}]

# Second example - Getting a specific entity and showing its connections
>>> c.get_entity(oid="ent13_0", params={"connections": True})
{'ts': [], 'location': [-20.799879, -42.117518], 'out': [], 'metadata': {'added': '2016-19-06T07:47:12.496944'}, 'data': {'contact': {'email': 'user@server.com'}, 'name': 'The Name of the Entity represented in this Object'}, 'ids': {'some_id': 'some_value'}, 'in': [], 'id': 'ent15_0', 'type': 'Empresa'}

# Third example - Getting entities with their connections using a query
>>> c.get_entity(params={"query": "(data:user)", "connections": True})
[{'ts': [], 'location': [-20.799879, -42.117518], 'out': [], 'metadata': {'added': '2016-19-06T07:47:12.496944'}, 'data': {'contact': {'email': 'user@server.com'}, 'name': 'The Name of the Entity represented in this Object'}, 'ids': {'some_id': 'some_value'}, 'in': [], 'id': 'ent15_0', 'type': 'Empresa'}]
```
**Important:** There are three types of returns in this endpoint. If the required search match a result in the database, then the endpoint will return either a `dict` (when the field **oid** is defined) or a `list` (otherwise). On the other hand, in the case of an HTTP error (like 404 for object not found or 500 for internal server error), the endpoint will return `False`.

### `get_relationship(oid=str(), params=dict())`
This endpoint performs searches on Relationship objects. The two required arguments are: an `str` which represents the **Relationship id** field (`oid`), usually in the format `relX_Y`, where `X` and `Y` are two integer numbers; and a `dict` which acts as a filter for the required search (`params`). The following code presents four examples of endpoint usage:

```python
# First example - Getting all relationships
>>> c.get_relationship()
[{'ts': [], 'out': 'ent13_0', 'metadata': {'added': '2016-07-18T18:01:47.496944'}, 'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'ids': {'code': '403984'}, 'in': 'ent15_0', 'id': 'rel14_1', 'type': 'Executora'}]

# Second example - Getting a specific relationship
>>> c.get_relationship(oid="rel14_0")
{'ts': [], 'out': 'ent13_0', 'metadata': {'added': '2016-07-18T18:01:47.496944'}, 'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'ids': {'code': '403984'}, 'in': 'ent15_0', 'id': 'rel14_1', 'type': 'Executora'}

# Third example - Getting relationships by type
>>> c.get_relationship(params={"type": "Executora"})
[{'ts': [], 'out': 'ent13_0', 'metadata': {'added': '2016-07-18T18:01:47.496944'}, 'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'ids': {'code': '403984'}, 'in': 'ent15_0', 'id': 'rel14_1', 'type': 'Executora'}]

# Fourth example - Getting relationships by a specific query
>>> c.get_relationship(params={"query": "(data:*)"})
[{'ts': [], 'out': 'ent13_0', 'metadata': {'added': '2016-07-18T18:01:47.496944'}, 'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'ids': {'code': '403984'}, 'in': 'ent15_0', 'id': 'rel14_1', 'type': 'Executora'}]
```
**Important:** The returns of this endpoint follow the same rules of the `get_entity()`. There will be returned a `dict` if the field **oid** is defined, or a `list` in other cases. It will be returned `False` in the case of some HTTP error.

### `get_timeserie(oid=str(), params=dict())`
This endpoint performs the search of TimeSerie objects. The two required arguments are: an `str` which represents the **TimeSerie id** field, which assumes the format `tsX_Y_Z`, where `X`, `Y` and `Z` are three integer numbers; and a `dict` which acts as a search filter for the required search. The following code presents two examples of usage for the endpoint:
```python
# First example - Getting all timeseries
>>> c.get_timeserie()
[{'desc': 'Esta é uma série de teste', 'tsid': 'ts15_0_0', 'name': 'Teste', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:48:26.184114', 'tags': ['tag1', 'tag2'], 'maxts': 0}, 'unit': 'Unidade'}, {'desc': 'Esta é a segunda série de teste', 'tsid': 'ts14_0_0', 'name': 'Teste2', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:50:03.168898', 'tags': ['tag2', 'tag3'], 'maxts': 0}, 'unit': 'Peso'}]

# Second example - Getting a list of timeseries with count limit
>>> c.get_timeserie(params={"limit": 1})
[{'desc': 'Esta é uma série de teste', 'tsid': 'ts15_0_0', 'name': 'Teste', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:48:26.184114', 'tags': ['tag1', 'tag2'], 'maxts': 0}, 'unit': 'Unidade'}, {'desc': 'Esta é a segunda série de teste', 'tsid': 'ts14_0_0', 'name': 'Teste2', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:50:03.168898', 'tags': ['tag2', 'tag3'], 'maxts': 0}, 'unit': 'Peso'}]

# Third example - Getting a list of timeseries using a query based on the name:
>>> c.get_timeserie(params={"name": "ts_*darksky*windspeed*"})
[{'desc': 'Esta é uma série de teste', 'tsid': 'ts15_0_0', 'name': 'ts_eol11_darksky_test_windspeed', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:48:26.184114', 'tags': ['tag1', 'tag2'], 'maxts': 0}, 'unit': 'Unidade'}, {'desc': 'Esta é a segunda série de teste', 'tsid': 'ts14_0_0', 'name': 'Teste2', 'info': {'npoints': 0, 'mints': 0, 'created': '2016-08-13T12:50:03.168898', 'tags': ['tag2', 'tag3'], 'maxts': 0}, 'unit': 'Peso'}]
```
**Important:** The types of return of this endpoint are the same of that in `get_relationship()`.

### `get_timeseries_sum(data=dict())`
This endpoint performs the sum of multiple timeseries points. The required argument `data` is a `dict` containing the three valid fields (i.e. `start`, `end` and `timeseries`). The following code presents an example for the endpoint:
```python
# Sum points from two timeseries from 2019-06-30 to 2019-07-01
>>> c.get_timeseries_sum(data={
        "start": "2019-06-30",
        "end": "2019-07-01",
        "timeseries": ["ts2746_6586_80", "ts2677_9946_0"]
    })
[{'failed': [], 'timeseries_sum': [[1561863600000, 390.0], [1561867200000, 665.0], [1561870800000, 665.0], [1561874400000, 540.0], [1561878000000, 390.0], [1561881600000, 390.0], [1561885200000, 390.0], [1561888800000, 390.0], [1561892400000, 465.0], [1561896000000, 465.0], [1561899600000, 465.0], [1561903200000, 465.0], [1561906800000, 465.0], [1561910400000, 465.0], [1561914000000, 465.0], [1561917600000, 465.0], [1561921200000, 465.0], [1561924800000, 465.0], [1561928400000, 502.5], [1561932000000, 590.0], [1561935600000, 502.5], [1561939200000, 465.0], [1561942800000, 465.0], [1561946400000, 390.0], [1561950000000, 390.0]]}]
```

### `get_file(oid=str(), params=dict())`
This endpoint performs the search of file objects. The two required arguments are: an `str` which represents the **file id** field, which assumes the format `fileX_Y`, where `X` and `Y` are two integer numbers; and a `dict` which acts as a search filter for the required search. The following code presents five examples of this endpoint:
```python
# First example - Capturing a specific file (by its id)
>>> c.get_file(oid='file1154_25346')
{'url': 'http://www2.aneel.gov.br/cedoc/dsp20161930ti.pdf', 'fileid': 'file1154_25346', 'downloaded': '2018-08-21T04:18:26.086000', 'checksum': '7b18a4974f4fad969113ca21a14aa59a', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/7b18a4974f4fad969113ca21a14aa59a/dsp20161930ti.pdf?Signature=gFZQzc27k%2FuiWDMC4%2FievOYZhbY%3D&Expires=1541895981&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'name': 'dsp20161930ti.pdf', 'lastupdate': '2018-08-21T04:18:26.086877', 'owners': ['ent177_136'], 'size': 114736, 'info': {'processed': True}}

# Second example - Capturing a specific file with its contents
>>> c.get_file(oid='file1154_25346', params={"contents": True})
{'url': 'http://www2.aneel.gov.br/cedoc/dsp20161930ti.pdf', 'fileid': 'file1154_25346', 'downloaded': '2018-08-21T04:18:26.086000', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/7b18a4974f4fad969113ca21a14aa59a/dsp20161930ti.pdf?Signature=gFZQzc27k%2FuiWDMC4%2FievOYZhbY%3D&Expires=1541895981&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'name': 'dsp20161930ti.pdf', 'info': {'processed': True, 'contents': {'miran_parser': {'pdfbox': 'AGÊNCIA NACIONAL DE ENERGIA ELÉTRICA – ANEEL \n \n \nDESPACHO Nº 1.930, DE 21 DE JULHO DE 2016 \nTexto Original  \n \nO SUPERINTENDENTE DE GESTÃO TARIFÁRIA DA AGÊNCIA NACIONAL DE \nENERGIA ELÉTRICA – ANEEL...'}}}, 'owners': ['ent177_136'], 'size': 114736, 'lastupdate': '2018-08-21T04:18:26.086877', 'checksum': '7b18a4974f4fad969113ca21a14aa59a'}

# Third example - Capturing a list of files with count limit
>>> c.get_file(params={'limit': 2})
[{'lastupdate': '2018-04-10T16:21:34.982552', 'size': 126368, 'info': {'description': '', 'user': {'email': 'joao@venidera.com', 'organization': 'Venidera', 'fullname': 'João Borsoi Soares'}, 'uploaded_at': '2018-04-10T19:21:43.572Z', 'processed': True, 'title': 'COLORTBL', 'mime_type': 'application/pdf', 'mobile': True}, 'name': 'colortbl.pdf', 'fileid': 'file1147_0', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/022509bae8af872f6a80f8a0bbe57c96/colortbl.pdf?Signature=4GSbH5tYcx1OXBlIzqvfApVNjyc%3D&Expires=1541895792&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'checksum': '022509bae8af872f6a80f8a0bbe57c96', 'owners': ['ent835_0'], 'url': '', 'downloaded': '2018-04-10T16:21:34.982000'}, {'lastupdate': '2017-11-27T11:25:59.784162', 'size': 24461425, 'info': {'description': '', 'user': {'organization': 'Venidera', 'fullName': 'Marcos de Almeida Leone Filho', 'userName': 'marcos@venidera.com'}, 'uploaded_at': '2017-11-27T13:26:35.992Z', 'processed': True, 'title': '2017-11-28-APRESENTACAO-ANEEL', 'mime_type': 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'mobile': True}, 'name': '2017-11-28-apresentacao-aneel.pptx', 'fileid': 'file1147_1', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/59d31dca02f61ad88fd9f8052f4ecb92/2017-11-28-apresentacao-aneel.pptx?Signature=wBR3zIKREk%2BsluhDKeZkCVh%2F2hc%3D&Expires=1541893635&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'checksum': '59d31dca02f61ad88fd9f8052f4ecb92', 'owners': ['ent835_0'], 'url': '', 'downloaded': '2017-11-27T11:25:59.784000'}]

# Fourth example - Capturing a list of files by type
>>> c.get_file(params={"type": "Infomercado_dados_individuais"})
[{'url': 'http://www.ccee.org.br/ccee/documentos/CCEE_636033', 'fileid': 'file1149_130718', 'name': 'InfoMercado Dados Individuais 2018.xlsx', 'checksum': '689114314131c350520f3c33594c0760', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/689114314131c350520f3c33594c0760/InfoMercado%20Dados%20Individuais%202018.xlsx?Signature=lDNqJ7iAl96A3VlXcsPm0j17iBw%3D&Expires=1541896451&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'lastupdate': '2018-11-10T01:17:18.507228', 'downloaded': '2018-11-10T01:17:19.491000', 'size': 37022238, 'info': {}, 'owners': ['ent121_4']}, {'url': 'http://www.ccee.org.br/ccee/documentos/CCEE_636033', 'fileid': 'file1154_129644', 'name': 'InfoMercado Dados Individuais 2018.xlsx', 'checksum': 'fe0fb3605f5cc01865101fcc765dbfdd', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/fe0fb3605f5cc01865101fcc765dbfdd/InfoMercado%20Dados%20Individuais%202018.xlsx?Signature=2B1XrnQPiZ94GDubGp3LciWM%2F64%3D&Expires=1541896451&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'lastupdate': '2018-10-12T01:16:13.325341', 'downloaded': '2018-10-12T01:16:13.325000', 'size': 33850646, 'info': {'processed': True}, 'owners': ['ent121_4']}, ...]

# Fifth example - Capturing files by a specific query
>>> c.get_file(params={"query": "(exceção OR exceções) AND NOT \"nenhuma exceção\""})
[{'lastupdate': '2016-09-26T07:31:02.764435', 'downloaded': '2016-09-26T07:31:02.764000', 'fileid': 'file1147_1277', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/f1192306e4a367c1c639fcc02ba299e8/rac_ap004_2004.pdf?Signature=k%2FWUahHqdSciSPawf8BiFGkl8Zc%3D&Expires=1541896636&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'checksum': 'f1192306e4a367c1c639fcc02ba299e8', 'owners': ['ent219_119'], 'size': 83688, 'info': {'resultados': True, 'spartafile': False, 'number': '2', 'processed': True, 'title': 'Relatório de Análise das Contribuições'}, 'url': 'http://www2.aneel.gov.br/aplicacoes/audiencia/arquivo/2004/004/resultado/rac_ap004_2004.pdf', 'name': 'rac_ap004_2004.pdf'}, {'lastupdate': '2016-09-26T07:29:07.926369', 'downloaded': '2016-09-26T07:29:07.926000', 'fileid': 'file1147_1299', 'url_storage': 'http://s3.amazonaws.com/files.venidera.net/msfiles/ca79e61c2238fc6f59e9fce2f6e129a4/resposta_%C3%A0s_questoes_metodologicas_sulgipe.pdf?Signature=sI6ExKtui0t7cGoENDgBB1T%2F2lI%3D&Expires=1541896636&AWSAccessKeyId=AKIAJYZCP2ZD2LUOO46A', 'checksum': 'ca79e61c2238fc6f59e9fce2f6e129a4', 'owners': ['ent218_74'], 'size': 20854, 'info': {'resultados': True, 'spartafile': False, 'number': '3', 'processed': True, 'title': 'Respostas às Questões Metodológicas.'}, 'url': 'http://www2.aneel.gov.br/aplicacoes/audiencia/arquivo/2004/039/resultado/resposta_%C3%A0s_questoes_metodologicas_sulgipe.pdf', 'name': 'resposta_às_questoes_metodologicas_sulgipe.pdf'}, ...]
```
**Important:** The types of return of this endpoint are the same of that in `get_timeserie()`.

### `get_points(oid=str(), params=dict())`
This endpoint looks for searching points of a specific timeserie. The two required arguments are: an `str` which represents the **TimeSerie tsid** field (`oid`); and a `dict` which acts as a search filter for the required search (`params`). The following code presents a example of usage for the endpoint:
```python
# First example - Getting all points related to the timeserie ts14_1_0 from 2015/01/01 to now
>>> c.get_points(oid="ts14_1_0", params={"start": "2015/01/01"})
{'values': [[24.0, 47.0, 66.0, 76.0, 1.0, 33.0, 2.0, 47.0, 17.0, 49.0, 84.0, 71.0]], 'timestamps': [['2015-01-01 00:00:00', '2015-01-02 00:00:00', '2015-01-03 00:00:00', '2015-01-04 00:00:00', '2015-01-05 00:00:00', '2015-01-06 00:00:00', '2015-01-07 00:00:00', '2015-01-08 00:00:00', '2015-01-09 00:00:00', '2015-01-10 00:00:00', '2015-01-11 00:00:00', '2015-01-12 00:00:00']]}
```
**Important:** This endpoint returns a `dict` containing a `list` of points for the related timeseries and their respectives timestamps, or `False` in the case of some HTTP error.

### `get_tags(params=dict())`
This endpoint aims to search tags by a query. The required argument is a `dict` with a key `q` and a velue representing the name of the tag. The following code presents a example of usage for the endpoint:
```python
>>> c.get_tags(params={"q": "tag2"})
[{'id': 'tag2', 'name': 'tag2', 'lenregs': 25, 'tag': 'tag2'}]
```
**Important:** This endpoint returns a `list` of tags for the related query or `False` in the case of some HTTP error.

### `get_entities_by_tag(tag=str())`
This endpoint returns a list of entities that contain a given tag. The required argument (`tag`) is a `str` representing the name of the tag. The following code presents a example of usage for the endpoint:
```python
>>> c.get_entities_by_tag(tag='ons_deck_decomp_2020_01')
[{'ids': {'identificador': 'ons_deck_decomp_2020_01'}, 'data': {'ano': '2020', 'mes': '01', 'title': 'Deck Decomp - Referência 2020/01.', 'description': ''}, 'metadata': {'tags': ['pmo', 'deck', 'decomp', 'deck_decomp', 'ons_deck_decomp', 'ons_deck_decomp_2020', 'ons_deck_decomp_2020_01'], 'webcrawler_id': 'ons_decomp'}, 'location': [-43.200946, -22.910215], 'registered': '2020-06-30T19:55:28.671118', 'lastupdate': '2020-06-30T19:55:28.671118', 'files': [], 'ts': [], 'id': 'ent712_1773', 'type': 'Deck_decomp'}]
```

### `get_entities_by_timeseries(tag=str())`
This endpoint returns a list of timeseries that contain a given tag. The required argument (`tag`) is a `str` representing the name of the tag. The following code presents a example of usage for the endpoint:
```python
>>> c.get_entities_by_timeseries(tag='ts_ons_geracao_horaria_verificada_14_de_julho_rs')
[{'desc': 'Geração horária verificada para a usina 14 DE JULHO-RS.', 'info': {'tags': ['ts_ons_geracao_horaria_verificada', 'ts_ons_geracao_horaria_verificada_14_de_julho_rs'], 'created': '2020-06-27T14:09:38.801905', 'lastupdate': '2020-07-03T13:24:40.514169'}, 'name': 'ts_ons_geracao_horaria_verificada_14_de_julho_rs', 'tsid': 'ts2327_2448_1', 'unit': 'MWh', 'obj_id': 'ent2327_2448'}]
```

### `add_entity(data=[dict()|list()], force=Bool())`
This endpoint inserts new Entity objects in the database. There are two arguments to be defined, where the first one is required and the second one is optional. They are described as follows:

 - `data`: This argument may be either a `dict` containing a valid Entity object structure or a `list` of `dict`, where each dict corresponds to a valid Entity object.
 - `force`: Corresponds to a `boolean` value that indicates whether a duplicate object must be added or not (default value is `false`).

If the objects were added successfully, then the endpoint will return a `list` with the entities added at time. However, in the case of an HTTP error for some request, the endpoint will return a `False` value for this request in the result list. The following code presents an example of inserting three Entity objects in the database.

```python
>>> ent1 = {
        "ids": {"entity": "1"},
        "data": {
            "name":"The Name of the Entity represented in this Object",
            "contact": {"email": "user@server.com"}
        },
        "metadata": {"added": "2016-19-06T07:47:12.496944"},
        "location": [-20.799879, -42.117518],
        "type": "Empresa"
    }
>>> ent2 = {
        "ids": {"entity": "2"},
        "data": {
            "name":"The Name of the Entity represented in this Object",
            "contact": {"email": "user@server.com"}
        },
        "metadata": {"added": "2016-19-06T07:47:12.496944"},
        "location": [-20.799879, -42.117518],
        "type": "Empresa"
    }
>>> ent3 = {
        "ids": {"entity": "3"},
        "data": {
            "name":"The Name of the Entity represented in this Object",
            "contact": {"email": "user@server.com"}
        },
        "metadata": {"added": "2016-19-06T07:47:12.496944"},
        "location": [-20.799879, -42.117518],
        "type": "Empresa"
    }
>>> ent_objects = c.add_entity(data=[ent1, ent2, ent3], force=False)
[{'ids': {'entity': '1'}, 'data': {'name': 'The Name of the Entity represented in this Object', 'contact': {'email': 'user@server.com'}}, 'registered': '2016-08-16 23:50:00.599128', 'id': 'ent13_0', 'lastupdate': '2016-08-16 23:50:00.599128', 'type': 'Empresa', 'ts': [], 'metadata': {'added': '2016-19-06T07:47:12.496944'}}, {'ids': {'entity': '2'}, 'data': {'name': 'The Name of the Entity represented in this Object', 'contact': {'email': 'user@server.com'}}, 'registered': '2016-08-16 23:50:00.660931', 'id': 'ent13_1', 'lastupdate': '2016-08-16 23:50:00.660931', 'type': 'Empresa', 'ts': [], 'metadata': {'added': '2016-19-06T07:47:12.496944'}}, {'ids': {'entity': '3'}, 'data': {'name': 'The Name of the Entity represented in this Object', 'contact': {'email': 'user@server.com'}}, 'registered': '2016-08-16 23:50:00.730125', 'id': 'ent13_2', 'lastupdate': '2016-08-16 23:50:00.730125', 'type': 'Empresa', 'ts': [], 'metadata': {'added': '2016-19-06T07:47:12.496944'}}]
```

### `add_relationship(data=[dict()|list()], force=Bool())`
This endpoint inserts new Relationship objects in the database. There is one required argument and one optional argument. They are described as follows:

 - `data`: This argument may be either a `dict` containing a valid Relationship object structure or a `list` of `dict`, where each dict corresponds to a valid Relationship object.
 - `force`: Corresponds to a `boolean` value that indicates whether a duplicate object must be added or not (default value is `false`).

If the objects were added successfully, then the endpoint will return a `list` with the relationships added at time. However, in the case of an HTTP error for some request, the endpoint will return a `False` value for this request in the result list. The following code presents an example of inserting two Relationship objects, where the first object has a invalid structure (there is no `metadata`).
```python
>>> rel1 = {
        "ids": {"rel-code": "1"},
        "data": {"desc": "Executora no projeto PAEWEB"},
        "type": "Executora",
        "from": "ent13_0",
        "to": "ent13_1"
    }
>>> rel2 = {
        "ids": {"rel-code": "2"},
        "data": {"desc": "Executora no projeto PAEWEB"},
        "metadata": {"added": "2016-08-13T15:01:47.496944"},
        "type": "Executora",
        "from": "ent13_0",
        "to": "ent13_1"
    }
>>> rel_objects = c.add_relationship(data=[rel1, rel2])
[False, {'ids': {'rel-code': '2'}, 'data': {'desc': 'Executora no projeto PAEWEB'}, 'out': 'ent13_0', 'registered': '2016-08-16 23:50:02.056719', 'id': 'rel14_6', 'lastupdate': '2016-08-16 23:50:02.056719', 'in': 'ent13_1', 'type': 'Executora', 'ts': [], 'metadata': {'added': '2016-08-13T15:01:47.496944'}}]
```

### `add_timeserie(data=dict())`
This endpoint inserts a new TimeSerie object in the database. There is one required argument to be defined. The required one (`data`) corresponds to a `dict` containing a valid TimeSerie object structure. If the object was added successfully, then the endpoint will return a `list` with the timeseries added at time. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents an example of inserting one TimeSerie object into the database.
```python
>>> obj = {
        "obj_id": "rel14_1",
        "name": "Cardio",
        "unit": "Pulse",
        "desc": "This is a cardio serie",
        "info": {
            "tags": ["man", "woman"]
        }
    }
>>> new_obj = c.add_timeserie(data=obj)
[{'modules': {}, 'name': 'Cardio', 'tsid': 'ts14_1_0', 'unit': 'Pulse', 'info': {'maxts': 0, 'created': '2016-08-13T15:38:35.174624', 'npoints': 0, 'tags': ['man', 'woman'], 'mints': 0}, 'desc': 'This is a cardio serie'}]
```

### `add_points(oid=str(), data=dict())`
This endpoint inserts a TSPoints object for a required timeseries in the database. There are two arguments that must be defined: the first one (`oid`) is a `str` that represents the id of a timeserie; the second one (`data`) corresponds to a `dict` containing a valid TSPoints object structure. If the object was added successfully, then the endpoint will return a `dict` with the status of all points. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents an example of inserting one TSPoints object into the database.
```python
>>> oid = "ts14_1_0"
>>> obj = {
        "timestamps": [1420077600, 1420164000, 1420250400, 1420336800,
                       1420423200, 1420509600, 1420596000, 1420682400],
        "values": [24.0, 47.0, 66.0, 76.0, 1.0, 33.0, 2.0, 47.0]
    }
>>> new_obj = c.add_points(oid=oid, data=obj)
{"failed": 0, "points": 12, "success": 12}
```

### `upd_entity(oid=str(), data=dict())`
This endpoint updates an Entity object in the database. There are two required arguments: an `str` which represents the id of the Entity object that will be changed and a `dict` with valid Entity values that will replace the old ones. If the object was replaced successfully, then the endpoint will return a `dict` with the new Entity object. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents a example of the endpoint usage:
```python
>>> oid = "ent14_0"
>>> obj = {
        "ids": {"code": "00000"},
        "location": [100, 100]
    }
>>> c.upd_entity(oid=oid, data=obj)
{'type': 'Executora', 'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'id': 'ent14_0', 'ts': [{'info': {'lastupdate': '2016-08-13T13:16:13.974470', 'maxts': 1421028000, 'mints': 1420077600, 'npoints': 24, 'tags': ['tag2', 'tag3'], 'created': '2016-08-13T12:50:03.168898'}, 'unit': 'Peso', 'modules': {}, 'desc': 'Esta é a segunda série de teste', 'key': 0, 'name': 'Teste2'}], 'ids': {'code': '0000'}, 'metadata': {'added': '2016-07-18T18:01:47.496944'}}
```

### `upd_relationship(oid=str(), data=dict())`
This endpoint updates an Relationship object in the database. The two required arguments are: an `str` which represents the id of the Relationship object that will be changed and a `dict` with valid Relationship values that will replace the old ones. If the object was replaced successfully, then the endpoint returns a `dict` with the new Relationship object. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents a example of the endpoint usage:
```python
>>> oid = "rel14_1"
>>> obj = {
        "ids": {
            "rel-code": "99999"
        }
    }
>>> c.upd_relationship(oid=oid, data=obj)
{'data': {'desc': 'Executora no projeto de pesquisa e desenvolvimento'}, 'id': 'rel14_1', 'metadata': {'added': '2016-07-18T18:01:47.496944'}, 'ids': {'code': '403984', 'rel-code': '99999'}, 'type': 'Executora', 'out': 'ent13_0', 'ts': [], 'in': 'ent15_0'}
```

### `upd_timeserie(oid=str(), data=dict())`
This endpoint updates a timeserie of an Entity or a Relationship object in the database. The two required arguments are: a `str` which represents the id of the TimeSerie object and a `dict` with valid timeserie structure that will replace the old one. If the object was replaced successfully, then the endpoint returns a `dict` containing the new TimeSerie object. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents a example of the endpoint usage:
```python
>>> oid = "ts15_0_0"
>>> obj = {
        "unit": "Other_unit",
        "name": "Other Name",
        "desc": "Other Description"
    }
>>> c.upd_timeserie(oid=oid, data=obj)
{"unit": "Other_unit", "name": "Other Name", "modules": {}, "desc": "Other Description", "key": 0, "tsid": "ts15_0_0", "info": {"created": "2016-12-15T14:15:57.719454","maxts": 0,"npoints": 0,"mints": 0, "tags": [ "tag2", "tag5"]}}
```

### `upd_file(oid=str(), data=dict())`
This endpoint updates a file of an Entity or a Relationship object in the database. The two required arguments are: a `str` which represents the id of the File object and a `dict` with values that will replace the old one. If the object was replaced successfully, then the endpoint returns a `dict` containing the new file object. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents a example of the endpoint usage:
```python
>>> oid = "file15_0_0"
>>> obj = {
        "url": "http://www.newurl.com.br",
        "info": {
            "key1": "value1"
        }
    }
>>> c.upd_file(oid=oid, data=obj)
{'size': 8597733, 'key': 0, 'downloaded': '2017-03-29T06:51:01.102819', 'name': 'resultado_consolidado_leiloes_publicacao_marco_2017.xlsm', 'url': 'http://www.newurl.com.br', 'checksum': 'a2728a852feee78bdcf5807b50f4d3ce', 'info': {'key1': 'value1'}, 'url_storage': 'http://venidera.net/vdsf/files/webdata/Leilao_resultado_consolidado/ent15_0/a2728a852feee78bdcf5807b50f4d3ce.xlsm'}
```


### `del_entity(oid=str(), params=dict())`
This endpoint deletes an Entity object, including all Relationship objects and time series associated to it. The first argument is a `str` which represents the id of the Entity object that will be deleted. The second argument is a `dict` with a valid **token**, i.e., a code used to ensure the deletion operation. The proccess of deleting an entity requires authentication. It means the endpoint `del_entity` must be called twice, where in the first call it is informed just the Entity id (`oid`) and in the second call it is informed both Entity id and a `dict` with the token. The response of the first call is a `dict` with the token id and expiration date or `False` in the case of some HTTP error. The response of the second call is `True` in the case of the entity was deleted, or `False` in the case of some HTTP error. The following code presents a example of the endpoint usage:

```python
>>> oid = "ent14_0"
>>> token = c.del_entity(oid=oid)
>>> if token:
>>>     c.del_entity(oid=oid, params={"token": token['token']['id']}
True
```

### `del_relationship(oid=str(), params=dict())`
This endpoint deletes a Relationship object, including all time series associated to it. The first argument is a `str` which represents the id of the Relationship object that will be deleted. The second argument is a `dict` with a valid **token**, i.e., a code used to ensure the deletion operation. The proccess of deleting a relationship requires authentication. It means the endpoint `del_relationship` must be called twice, where in the first call it is informed just the Relationship id (`oid`) and in the second call it is informed both Relationship id and a `dict` with the token. The response of the first call is a `dict` with the token id and expiration date or `False` in the case of some HTTP error. The response of the second call is `True` in the case of the relationship was deleted, or `False` in the case of some HTTP error. The following code presents a example of the endpoint usage:

```python
>>> oid = "rel16_1"
>>> token = c.del_relationship(oid=oid)
>>> if token:
>>>     c.del_relationship(oid=oid, params={"token": token['token']['id']}
True
```

### `del_timeserie(oid=str(), params=dict())`
This endpoint deletes a time series of an Entity or a Relationship object, including all points associated to it. The first argument is a `str` which represents the id of the time series that will be deleted (e.g. `tsX_Y_Z`). The second argument is a `dict` with a valid **token**, i.e., a code used to ensure the deletion operation. The proccess of deleting a time series requires authentication. It means the endpoint `del_timeserie` must be called twice, where in the first call it is informed just the time series id (`oid`) and in the second call it is informed both time series id and a `dict` with the token. The response of the first call is a `dict` with the token id and expiration date or `False` in the case of some HTTP error. The response of the second call is `True` in the case of the time series was deleted, or `False` in the case of some HTTP error. The following code presents a example of the endpoint usage:

```python
>>> oid = "ts16_1_0"
>>> token = c.del_timeserie(oid=oid)
>>> if token:
>>>     c.del_timeserie(oid=oid, params={"token": token['token']['id']})
True
```


### `del_file(oid=str(), params=dict())`
This endpoint deletes a file object of an Entity or a Relationship object, **but not the physical files associated to it**. The first argument is a `str` which represents the file id that will be deleted (e.g. `fileX_Y_Z`). The second argument is a `dict` with a valid **token**, i.e., a code used to ensure the deletion operation. The proccess of deleting a file requires authentication. It means the endpoint `del_file` must be called twice, where in the first call it is informed just the file id (`oid`) and in the second call it is informed both file id and a `dict` with the token. The response of the first call is a `dict` with the token id and expiration date or `False` in the case of some HTTP error. The response of the second call is `True` in the case of the file was deleted, or `False` in the case of some HTTP error. The following code presents a example of the endpoint usage:

```python
>>> oid = "file14_0_2"
>>> token = c.del_file(oid=oid)
>>> if token:
>>>     c.del_file(oid=oid, params={"token": token['token']['id']}
True
```

### `del_points(oid=str(), params=dict())`
This endpoint deletes points of a time series. The first argument is a `str` which represents the id of the time series whose points will be deleted. The second argument is a `dict` with must contain a valid **token** and also two optional arguments: a start and an end date (in the ISO format). The proccess of deleting points requires authentication. It means the endpoint `del_points` must be called twice, where in the first call it is informed just the time series id (`oid`) and in the second call it is informed both time series id and a `dict` with the token (with the range of deletion or not). The response of the first call is a `dict` with the token id and expiration date or `False` in the case of some HTTP error. The response of the second call is `True` in the case of the points of the time series were deleted, or `False` in the case of some HTTP error. The following code presents a example of the endpoint usage:

```python
>>> oid = "ts16_1_0"
>>> token = c.del_points(oid=oid)
>>> if token:
>>>     c.del_points(oid=oid, params={"token": token['token']['id'], "start": "2015/01/01", "end": "2016/08/10"}
True
```

### `upload_file(oid=str(), pfrom=str(), pinfo=dict(), session=None)`
This endpoint uploads a new file in the database. The two required arguments are: an `oid` which represents the id of an Entity or a Relationship object where the file will belong, and `pfrom`, i.e., the **web or local address** from where the file will be captured. There are also two optional arguments: `pinfo`, a `dict` which contains any additional information regarding the uploaded file and `session`, a [Python requests Session object](http://docs.python-requests.org/en/master/user/advanced) which may be used for uploading protected files. If the file was uploaded successfully, then the endpoint will return a `dict` with the following file information: `fileid`, `checksum`, `size`, `downloaded`, `url_storage`, `name`, `info` and `url`. On the other hand, in the case of an HTTP error, the endpoint will return `False`. The following code presents an example of upload one file into the database.
```python
# First example - Uploading a file with a local address
>>> c.upload_file(oid="ent14_0", pfrom="/home/rafael/teste.arq")
{'url_storage': 'http://venidera.net/vdsf/files/webdata/Infomercado_dados_individuais/ent14_0/b9155515728fa0f69d9770f7877cb50a.arq', 'size': 36, 'name': 'teste.arq', 'info': {}, 'downloaded': '2017-04-03T17:34:02.533510', 'checksum': 'b9155515728fa0f69d9770f7877cb50a', 'url': '', 'fileid': 'file14_0_1'}

# Second example - Uploading a file with a web address
>>> c.upload_file(oid="ent14_1", pfrom="http://www.ccee.org.br/ccee/documentos/CCEE_386657")
{"url_storage": "http://venidera.net/vdsf/files/webdata/Central_geradora_eolica/ent14_0/44417d1ca6a5a196f4e5f40b131f6241.xlsx", "name": "InfoMercado Dados Individuais 2017.xlsx", "info": {}, "checksum": "44417d1ca6a5a196f4e5f40b131f6241", "url": "http://www.ccee.org.br/ccee/documentos/CCEE_386657", "downloaded": "2017-04-03T03:08:03.981601", "size": 6509947, "fileid": "file14_1_3"}
```

### `download_file(oid=str(), pto=str(), hashname=bool())`
This endpoint performs the download of a file from a specific object. The two required arguments are: an `oid` which represents the id of the required file object, and a path `pto`, i.e., the path where the file obtained from the `oid` will be stored. Also, it is possible to inform an optional argument named `hashname`, i.e., a `boolean` value (default `False`) that indicates if the name of the file would be replaced by an `uuid4()` value. If the file was downloaded successfully, then the endpoint will return a `śtr` with the current file path. On the other hand, the endpoint will return `False`. The following code presents two examples of usage for the endpoint:
```python
# First example - Downloading a file with its original name
>>> resp = c.download_file(oid="file17_0_0", pto="/home/rafael/")
/home/rafael/aviso_dou_ap009_2017_29.3.17_p92.pdf

# Second example - Downloading a file with a hash name
>>> resp = c.download_file(oid="file17_0_0", pto="/home/rafael/", hashname=True)
/home/rafael/9c554d92-3197-478d-83f5-ebee49ae7f56.pdf
```

### `download_files(oids=list(), pto=str(), hashname=bool(), threading=bool())`
This endpoint performs the download of multiple files. The two required arguments are: an `oids` which represents the ids of the required file objects, and a path `pto`, i.e., the path where the files obtained from the `oids` will be stored. Also, it is possible to inform two optional arguments: `hashname`, i.e., a `boolean` value (default `False`) that indicates if the name of the file would be replaced by an `uuid4()` value and `threading`, i.e., a `boolean` value (default `True`) that indicates either the files will be downloaded in parallel or not. If the files were downloaded successfully, then the endpoint will return a `list` with the current file paths. The following code presents two examples of usage for the endpoint:
```python
# First example - Downloading files with their original name
>>> resp = c.download_files(oids=["file380_3799", "file380_3798"], pto="/home/rafael/")
['/home/rafael/903751665526.pdf', '/home/rafael/901600676110.pdf']

# Second example - Downloading files with hash names
>>> resp = c.download_files(oids=["file380_3799", "file380_3798"], pto="/home/rafael/", hashname=True)
["/home/rafael/01610f52-c5c1-41ed-a557-a621cb82d5d3.pdf", "/home/rafael/f58c1a0c-5a2a-49ec-9cd4-76d74960e3ac.pdf"]
```

### `link_file(data=dict())`
This endpoint created a link between a file and an object in the database. The `data` argument is a `dict` containing as keys the target `obj_id` and `fileid`. If the link was created successfully, then the endpoint will return a success message. However, in the case of an HTTP error for some request, the endpoint will return `False`. The following code presents an example of this method:

```python
>>> c.link_file(data={
	    "fileid": "file520_3732",
	    "obj_id": "ent170_21"
    })
Link created between file520_3732 and ent170_21.
```

### `unlink_file(data=dict())`
This endpoint removes a link between a file and an object in the database. The `data` argument is a `dict` containing as keys the target `obj_id` and `fileid`. If the link was removed successfully, then the endpoint will return a success message. However, in the case of an HTTP error for some request, the endpoint will return `False`. The following code presents an example of this method:

```python
>>> c.unlink_file(data={
	    "fileid": "file520_3732",
	    "obj_id": "ent170_21"
    })
Link removed from file520_3732 and ent170_21.
```

## Applications

Some methods were added in barrel_client to support access applications that were built on top of barrel. An application object will be managed only through its methods, so barrel will avoid to operate applications objects.

### `add_notifications_modules(name='', description='', location=None, httpresp=False)`
This method will add a notification module that works as grouper for notifications. The required fields are *name* and *description*. A location can be provided by sending a list with longitude and latitude (Example: *location=[-42.00, -22.00]*), requests that don't inform a location will be defined with the location of Venidera's office ([-47.0878913, -22.893313]). All notifications of a group will inherite its location. The argument *httpresp* will return the complete API response when defined as *True*.
A module name will be spaces replaced by an underscore character also it will have its first letter as capital and the rest will be in lowercase.

```python
>>> resp = c.add_notifications_modules(name='Modulo PLD', description='Notificações do módulo de PLD.')
>>> print(resp)
[{'registered': '2017-07-04T10:52:57.524754', 'name': 'Modulo_pld', 'description': 'Notificações do módulo de PLD.', 'id': 'nm791_1', 'location': [-47.0878913, -22.893313], 'lastupdate': '2017-07-04T10:52:57.524754'}]
```

Note the module name entered and the final created. All notification modules will be identified by an unique *id* that can be used to request information about a specific module. Take a look below.

### `get_notifications_modules(oid="", params=None, httpresp=False)`
This method provides reading for notification module objects. if an *oid* is provided it shows the object related otherwise will return a list with all notification modules objects paginated and limited by query arguments(*skip* and *limit*).

```python
>>> resp = c.get_notifications_modules()
>>> for i in resp:
...     print(i)
{'description': 'Notificações do módulo de PLD.', 'location': [-47.0878913, -22.893313], 'lastupdate': '2017-07-04T10:52:57.524754', 'registered': '2017-07-04T10:52:57.524754', 'name': 'Modulo_pld', 'id': 'nm791_1'}
# Now call only the specific module
>>> resp = c.get_notifications_modules(oid='nm791_1')
>>> print(resp)
{'description': 'Notificações do módulo de PLD.', 'location': [-47.0878913, -22.893313], 'lastupdate': '2017-07-04T10:52:57.524754', 'registered': '2017-07-04T10:52:57.524754', 'name': 'Modulo_pld', 'id': 'nm791_1'}
```

### `add_notification(module='', notification='', weight=1, objects_related=[], category='INFO', httpresp=False)`
This method allows notifications inserting. A notification requires a module and a message, other fields can use default values as defined in the method header showed above.
A *weight* can be define to emphasize the notification for algorithms that will use its data. A list of *objects_related* can be passed to link the notification with data objects from barrel. A *category* can be defined using admissible values from the list *['ERROR', 'WARNING', 'INFO', 'DEBUG', 'FAIL']*, if other value is passed as argument it will be converted to *'INFO'*.

```python
>>> resp = c.add_notification(module='Modulo PLD', notification='Notificação passada com o nome do módulo contendo espaço.', weight=5)
>>> print(resp)
[{'location': [-47.0878913, -22.893313], 'notification': 'Notificação passada com o nome do módulo contendo espaço.', 'objects_related': [], 'lastupdate': '2017-07-04T11:21:49.534470', 'weight': 5, 'category': 'INFO', 'registered': '2017-07-04T11:21:49.534470', 'module': 'Modulo_pld', 'clicks': 0, 'id': 'not819_0'}]
>>> resp = c.add_notification(module='Modulo PLD', notification='Notificação passada com o nome do módulo contendo espaço.', weight=2, category='invalid')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/Users/andre/git/barrel_client/test_dev/venv-mac/lib/python3.6/site-packages/barrel_client-0.0.1-py3.6.egg/barrel_client/client.py", line 681, in add_notification
AssertionError: Fied <category> must be one of these: ['ERROR', 'WARNING', 'INFO', 'DEBUG', 'FAIL']
>>> resp = c.add_notification(module='Modulo PLD', notification='Notificação passada com o nome do módulo contendo espaço.', weight=2, category='INFO')
>>> print(resp)
[{'location': [-47.0878913, -22.893313], 'notification': 'Notificação passada com o nome do módulo contendo espaço.', 'objects_related': [], 'lastupdate': '2017-07-04T11:26:05.671427', 'weight': 2, 'category': 'INFO', 'registered': '2017-07-04T11:26:05.671427', 'module': 'Modulo_pld', 'clicks': 0, 'id': 'not820_0'}]
```

### `get_notifications(module="", params=None, httpresp=False)`
This method provides reading for notifications. A module name must be passed as requirement to read the notifications. It also implements pagination features. The order of the notification list will be descending.

```python
>>> resp = c.get_notifications(module='Modulo_pld')
>>> for i in resp:
...     print(i)
{'weight': 2, 'category': 'INFO', 'registered': '2017-07-04T11:26:05.671427', 'module': 'Modulo_pld', 'location': [-47.0878913, -22.893313], 'clicks': 0, 'notification': 'Notificação passada com o nome do módulo contendo espaço.', 'objects_related': [], 'lastupdate': '2017-07-04T11:26:05.671427', 'id': 'not820_0'}
{'weight': 5, 'category': 'INFO', 'registered': '2017-07-04T11:21:49.534470', 'module': 'Modulo_pld', 'location': [-47.0878913, -22.893313], 'clicks': 0, 'notification': 'Notificação passada com o nome do módulo contendo espaço.', 'objects_related': [], 'lastupdate': '2017-07-04T11:21:49.534470', 'id': 'not819_0'}
```

### `consulta_miran_web(data=dict())`
This endpoint performs a query on Miran Web. The required argument (`data`) corresponds to a `dict` containing a valid Miran Geo query object structure. If the data is valid, then the endpoint will return a `dict` with the Miran Web query result. On the other hand, the endpoint will return `False`. The following code presents an example of querying on Miran Web.
```python
>>> obj = [
        'version': '...',
        ...
        'consults': {
            "type": "group",
            "combinador": {
                "type": 1,
                "name": "E"
            },
            "agregador": {
                "type": 1,
                "valor": "Soma"
            },
            "text": "Grupo xxxx",
            "tags": [
                {
                    "id": "ts_ons_dessem_pdo_operacao",
                    "name": "ts_ons_dessem_pdo_operacao",
                    "tag": "ts_ons_dessem_pdo_operacao",
                    "lenregs": 59,
                    "show.tag.detail": False
                },
                ...
                {
                    "id": "ts_ons_dessem_2019_08",
                    "name": "ts_ons_dessem_2019_08",
                    "tag": "ts_ons_dessem_2019_08",
                    "lenregs": 27,
                    "show.tag.detail": False
                }
            ],
            "id": 0,
            "name": "Geração - DESSEM",
            "bounds": None,
            "stacking": None,
            "stack": "Grupo 0",
            "color": "rgb(238, 57, 49)",
            "visible": True
        }
    ]
>>> c.consulta_miran_web(data=data)
{'group': {'0': {'entities': [...], 'entities_count': 27, 'timeseries': [...], 'timeseries_count': 27, 'relationship_types_found': [], 'timeseries_from_relationships': []}}}
```

## Troubleshooting

### Fail to install on Ubuntu
If you are using Ubuntu, you may face the following error when installing the barrel_client package over Python 3.5:
```
gevent/gevent.corecext.c:5:20: fatal error: Python.h: No such file or directory
```
To solve this problem, just install the `python3-dev` package on your machine and try installing barrel_client again.

### Fail to install due to *UnicodeDecodeError: 'ascii'*

Suppose you are executing the command below and got the same error:

```
(venv-3.6) andre@host $ pip install git+ssh://git@bitbucket.org/venidera/barrel_client.git --upgrade
Collecting git+ssh://git@bitbucket.org/venidera/barrel_client.git
  Cloning ssh://git@bitbucket.org/venidera/barrel_client.git to /tmp/pip-9gpoayu2-build
    Complete output from command python setup.py egg_info:
    warning: pypandoc module not found, could not convert Markdown to RST
    Traceback (most recent call last):
    ...
    UnicodeDecodeError: 'ascii' codec can't decode byte 0xc3 in position 4149: ordinal not in range(128)
```

It means that the *README.md* has *ascii* characters. In order to solve it, change your environment locale in session doing:

```
(venv-3.6) andre@host $ export LANG=pt_BR.utf8
(venv-3.6) andre@host $ export LC_ALL=pt_BR.utf8
```

Then repeat the install command.
This configuration will be used only on the active terminal session.
A pythonic and definitive way to solve this problem is control the locale inside the program. It can be done with:

```
import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.utf-8')
```

## Contributing and Issues

Fork the repository and do a pull request.
Please file a BitBucket issue to [report a bug](https://bitbucket.org/venidera/barrel_client/issues?status=new&status=open).

## Maintainers

[Venidera Research and Development](http://portal.venidera.com) team:

* Andre E. Toscano - [rafael@venidera.com](mailto:rafael@venidera.com)

## License

This package is released and distributed under the license [GNU GPL Version 3, 29 June 2007](https://www.gnu.org/licenses/gpl-3.0.html).
