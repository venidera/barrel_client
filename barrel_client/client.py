# !/usr/bin/env python3

import os
import cgi
import stat
import time
import socket
import shutil
import logging
import inspect
import urllib3
import os.path
import certifi
import requests
import tempfile

import ntpath as nt
from uuid import uuid4
from hashlib import md5
from io import StringIO
from pathlib import Path
from functools import wraps
from datetime import datetime
from mimetypes import MimeTypes
from urllib.parse import urlsplit
from joblib import Parallel, delayed
from urllib.request import urlretrieve
from json import dumps as tdumps, loads

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

try:
    import magic
except Exception:
    pass


def joblib_file_download(self, oid="", pto=None, hashname=False):
    assert isinstance(oid, str), 'Field <oid> must be an oid.'
    assert isinstance(pto, str), 'Field <pto> must be a string.'
    assert isinstance(hashname, bool), 'Field <hashname> must be a bool.'

    dirname = nt.dirname(pto)
    assert nt.isdir(dirname), \
        'The path <%s> is not a valid directory.' % dirname
    assert os.access(dirname, os.W_OK), \
        'There are no permissions to write files in <%s>.' % dirname

    try:
        # Checking if file exists
        r = self.get(endpoint="files", oid=oid)
        if r and 'data' in r:
            # Content header filename and extension
            hname = r['data']['name']
            hext = hname.split(".")[-1]
            filename = "%s.%s" % (uuid4(), hext) if hashname else hname
            fullpath = "%s/%s" % (dirname, filename)
            # Downloading file
            resp = self.http.request('GET', r['data']['url_storage'])
            # Writing file to file system
            with open(fullpath, 'wb') as w:
                w.write(resp.data)
            # Checking whether file was downloaded
            assert os.path.isfile(fullpath), \
                'The file was not stored in the correct path.'
            # Checking whether file is not corrupted
            assert os.stat(fullpath).st_size > 0, \
                'The size of file is too small.'
            # Checking if the checksum is the same
            with open(fullpath, 'rb') as r:
                chk = md5(r.read()).hexdigest()
                if chk != md5(r.read()).hexdigest():
                    assert 'The checksum of file is different.'
            # Returning the path
            return fullpath
        else:
            return r
    except Exception:
        return False

def ping(host, port):
    try:
        logging.info('Ping in {}:{} - Barrel Server: Ok\n'.format(host, port))
        socket.socket().connect((host, port))
        return True
    except socket.error as err:
        if err.errno == socket.errno.ECONNREFUSED:
            logging.info('Can\'t connect to Barrel Server')
            return False
        raise Exception('Fail to test Barrel Server connection status')


def retry(cls):
    # Criando decorator para função
    def decorator_for_func(orig_func):
        @wraps(orig_func)
        def decorator(*args, **kwargs):
            for attempt in range(6):
                try:
                    return orig_func(*args, **kwargs)
                except AssertionError as exc:
                    raise AssertionError(exc)
                except Exception as exc:
                    logging.warning(
                        'Houve um erro na requisição: %s' % str(exc))
                    logging.info('Tentando novamente em %d segundos.' % (
                        3**attempt))
                    time.sleep(3**attempt)
            return orig_func(*args, **kwargs)
        return decorator
    # Adicionando decorator em todos os métodos da classe
    for name, method in inspect.getmembers(cls):
        if (not inspect.ismethod(method) and not
            inspect.isfunction(method)) or \
                inspect.isbuiltin(method) or name.startswith('_'):
            continue
        if name in ['get', 'post', 'put', 'delete']:
            continue
        setattr(cls, name, decorator_for_func(method))
    return cls


@retry
class Connection(object):

    def __init__(self, server='192.168.40.10', port=9090):

        # Creating alias to keep compatibility
        self.get_timeserie = self.get_timeseries
        self.add_timeserie = self.add_timeseries
        self.upd_timeserie = self.upd_timeseries
        self.del_timeserie = self.del_timeseries
        self.get_timeserie_sum = self.get_timeseries_sum

        # urllib3 poolmanager
        self.http = urllib3.PoolManager(
            num_pools=10,
            timeout=urllib3.Timeout(connect=1.0, read=2.0),
            retries=urllib3.Retry(30, redirect=5, raise_on_redirect=False),
            ca_certs=certifi.where()
        )
        if 'https' not in server:
            self.server = server
            self.port = port
        else:
            srv = server.split('//')[1]
            if '/' in srv:
                srv = srv.split('/')[0]
            self.server = srv
            self.port = 443
        self.prefix = '/api/v0'
        self.skip = 500
        self.headers = {'content-type': "application/json"}
        try:
            self.home_dir = str(Path.home()) + '/.Venidera/'
        except Exception as e:
            logging.info(e)
            logging.info('home() not found in path...')
            self.home_dir = str(os.path.expanduser('~')) + '/.Venidera/'

        self.credential_file = self.home_dir + 'barrel_client.credential'
        self.token = None
        self.logged = False
        if Path(self.credential_file).exists():
            with open(self.credential_file) as fcred:
                self.token = fcred.read()
        if ping(self.server, self.port):
            if not self.check_credentials():
                logging.info('Credentials are required. Execute the method do_login.')
            else:
                return

    def api_login(self, username=None, password=None):
        # Checking username and password
        assert isinstance(
            username, str), 'Field <username> must be provide as a string.'
        assert isinstance(
            password, str), 'Field <password> must be provide as a string.'
        # Creating list if addresses
        ac_addresses = [
            'http%s://%s:%d/api/v0/login' % (
                's' if self.port == 443 else '', self.server, self.port),
            'https://miran-barrel.venidera.net/api/v0/login',
            'https://miran-api.venidera.net/login'
        ]
        # Iterating over addresses
        for ac_address in ac_addresses:
            try:
                r = requests.post(
                    ac_address,
                    data=tdumps({
                        'username': username,
                        'password': password
                    }), headers=self.headers, proxies={
                        'http': None, 'https': None
                    }
                )
                if r and r.status_code in [200, 201]:
                    self.logged = True
                    self.headers['Venidera-AuthToken'] = r.json()['data']['token']
                    # Letting username global
                    self.username = username
                    logging.info('Connected to %s.' % ac_address)
                    return True
            except Exception:
                logging.info('Could not connect to %s. ' % ac_address)
        return False

    def do_login(self, username=None, password=None, auth=True):
        # Calling api_login
        return self.api_login(username=username, password=password)

    def check_credentials(self):
        if self.token:
            self.headers['Venidera-AuthToken'] = self.token
        else:
            return False
        query = self.get_url() + self.get_endpoint('version')
        r = requests.get(query, verify=False, headers=self.headers, proxies={
            'http': None, 'https': None
        })
        if r is not None and r.status_code == 200:
            return True
        else:
            logging.info('Invalid credentials. Execute the do_login method.')
            return False

    def is_logged(self):
        return self.logged

    def api_logout(self):
        self.token = None
        if 'Venidera-AuthToken' in self.headers.keys():
            del self.headers['Venidera-AuthToken']
        self.logged = False
        try:
            os.remove(self.credential_file)
        except:
            pass

    def do_logout(self):
        self.api_logout()

    def get_endpoint(self, key):
        # The expression below only have ! to specific cases
        return self.prefix + {
            'pts': '/timeseries/!/points',
            'ent': '/entities/',
            'entsearch': '/entities/search/',
            'entbatch': '/entities/batch/',
            'enttypes': '/entities/types/',
            'entupdate': '/entities/!',
            'text': '/text/',
            'textsearch': '/text/search/',
            'textupdate': '/text/!',
            'rel': '/relationships/',
            'relsearch': '/relationships/search/',
            'relbatch': '/relationships/batch/',
            'reltypes': '/relationships/types/',
            'relupdate': '/relationships/!',
            'tsr': '/timeseries/',
            'tsrsum': '/timeseries/sum',
            'tsrupdate': '/timeseries/!',
            'tssearch': '/timeseries/search/',
            'version': '/version/',
            'types': '/types/',
            'files': '/files/',
            'fileupdate': '/files/!',
            'filesearch': '/files/search/',
            'linkfile': '/files/link',
            'unlinkfile': '/files/unlink',
            'tags': '/tags/',
            'obtags': '/entities/tags/',
            'tstags': '/timeseries/tags/',
            'notmod': '/notifications/modules/',
            'notmess': '/notifications/messages/',
            'orgs': '/organizations/'
        }.get(key)

    def get(self, endpoint=None, oid="", params=None):
        assert self.is_logged(), 'You must login to use barrel client.'
        params = params or {}
        if not endpoint:
            return

        ep = self.get_endpoint(endpoint).replace("!", oid)
        query = self.get_url() + ep + ("" if any(
            [i in endpoint for i in ["pts", "download"]]) else oid)

        # Setting the size of each window [None is 500]
        if "pagesize" not in params.keys():
            params["skip"] = 1000
        else:
            assert isinstance(params["pagesize"], int), \
                "The field <pagesize> must be an int."
            assert 0 < params["pagesize"], \
                "The field <pagesize> must be greater than zero."
            params["skip"] = params["pagesize"]
            del params["pagesize"]

        # Setting the number of documents to be retrived [None is ALL]
        if "limit" not in params.keys():
            params["limit"] = None
        else:
            assert isinstance(params["limit"], int), \
                "The field <limit> must be an int."
            assert params["limit"] > 0, \
                "The field <limit> must be greater than zero."

        # Setting the required page [None is 1]
        if "page" not in params.keys():
            params["page"] = 1
        else:
            assert isinstance(params["page"], int), \
                "The field <page> must be an int."

        fresp = {}
        skip = params["skip"]
        limit = params["limit"]
        params["skip"] = 0
        params["limit"] = min(skip, skip + 1 if limit is None else limit)
        while True:
            if params["page"] != 1:
                params["skip"] = skip * (params["page"] - 1)

            temp_page = params["page"]
            del params["page"]
            r = requests.get(query, verify=False, params=params, 
                headers=self.headers, proxies={
                    'http': None, 'https': None
                }
            )
            params["page"] = temp_page

            if r.status_code >= 500:
                break
            presp = r.json()
            try:
                presp['status_code'] = r.status_code
            except:
                pass

            if "data" not in presp.keys():
                return fresp or presp

            if not fresp:
                fresp = presp
                if len(fresp['data']) != skip:
                    return fresp
            else:
                fresp['data'].extend(list(presp['data']))

            params['skip'] += skip
            if limit is not None:
                params['limit'] = min(
                    params['limit'], limit - params['skip'])
            if len(fresp['data']) < params['skip'] \
                    or params["page"] != 1:
                break

        return fresp

    def post(self, endpoint=None, data=[], oid="", force=False):
        assert self.is_logged(), 'You must login to use barrel client.'
        if not endpoint or not data:
            return

        if 'batch' in endpoint:
            url = self.get_url() + self.get_endpoint(endpoint) + (
                '?force=true' if force else '')
            output = StringIO()
            output.write(self.dumps(data))
            files = {'files': output.getvalue()}
            return [requests.post(url, files=files, headers=self.headers, proxies={
                'http': None, 'https': None
            })]
        else:
            ep = self.get_endpoint(endpoint).replace("!", oid)
            if force:
                ep += '?force=true'

            r = []
            for req in data:
                r.append(
                    requests.post(
                        self.get_url() + ep, 
                        data=self.dumps(req), 
                        headers=self.headers, 
                        proxies={
                            'http': None, 'https': None
                        }
                    )
                )

        return r

    def put(self, endpoint=None, oid="", data=dict(), params=dict()):
        assert self.is_logged(), 'You must login to use barrel client.'
        if not endpoint or not bool(data) or oid == "":
            return

        ep = self.get_endpoint(endpoint).replace("!", oid)
        r = requests.put(
            self.get_url() + ep, data=self.dumps(data), headers=self.headers, 
            proxies={
                'http': None, 'https': None
            }, params=params
        )
        return r

    def delete(self, endpoint=None, oid="", params=None):
        assert self.is_logged(), 'You must login to use barrel client.'
        params = params or {}
        if not endpoint:
            return

        ep = self.get_endpoint(endpoint).replace("!", oid)
        query = self.get_url() + ep + ("" if "pts" in endpoint else oid)
        r = requests.delete(query, params=params, headers=self.headers, proxies={
            'http': None, 'https': None
        })
        return r

    def process_response(self, response, op=None):
        if isinstance(response, dict):
            r = response["data"] if "data" in response.keys() else False
            if isinstance(r, list) and op == 'batch':
                return [self.process_response(d) for d in r]
            else:
                return r
        elif isinstance(response, list):
            return [self.process_response(r) for r in response]
        elif isinstance(response, str):
            return response
        elif response in [False, None]:
            return False
        else:
            status = response.status_code

            if status == 400:
                try:
                    data = response.json()
                    return data["data"] if "data" in data.keys() else False
                except Exception:
                    pass

            if status not in [401, 409] and not (200 <= status < 300):
                return False
            if op in ['del'] and status != 401:
                return True

            data = response.json()
            return data["data"] if "data" in data.keys() else data['message']

    def get_url(self):
        if self.port == 443:
            return 'https://' + self.server
        return 'http://%s:%d' % (self.server, self.port)

    def get_tags(self, params=None, httpresp=False):
        r = self.get(endpoint="tags", params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_entities_by_tag(self, tag=None, httpresp=False):
        assert isinstance(tag, str), 'Field <tag> must be a string.'
        r = self.get(endpoint="obtags", oid=tag)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_timeseries_by_tag(self, tag=None, httpresp=False):
        assert isinstance(tag, str), 'Field <tag> must be a string.'
        r = self.get(endpoint="tstags", oid=tag)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False


    def get_organizations(self, params=None, httpresp=False):
        r = self.get(endpoint="orgs", params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_version(self):
        return self.get(endpoint="version")

    def get_types(self, otype="", params=None, httpresp=False):
        r = self.get(endpoint="types", oid=otype, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_entity_types(self, params=None, httpresp=False):
        r = self.get(endpoint="enttypes", params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_relationship_types(self, params=None, httpresp=False):
        r = self.get(endpoint="reltypes", params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_text(self, oid="", params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "textsearch" if "query" in params.keys() else "text"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_entity(self, oid="", params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "entsearch" if "query" in params.keys() else "ent"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_relationship(self, oid="", params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "relsearch" if "query" in params else "rel"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_timeseries(self, oid="", params=dict(), httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = 'tssearch' if 'query' in params or 'name' in params else 'tsr'

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_file(self, oid="", params=dict(), httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "filesearch" if "query" in params.keys() else "files"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_timeseries_sum(self, data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert all(
            [k in data.keys() for k in ['start', 'end', 'timeseries']]), \
            'Data must contain start date, end date and time series.'

        assert isinstance(data['timeseries'], list), \
            'Field <timeseries> must be a list.'
        assert len(data['timeseries']) > 0, \
            'The size of <timeseries> must be greater than zero.'
        for d in data['timeseries']:
            assert isinstance(d, str), \
                'The element of the <timeseries> field must be a string.'

        if isinstance(data, dict):
            data = [data]

        for _ in range(100):
            r = self.post(endpoint="tsrsum", data=data)
            if r[0].status_code == 200:
                break
            time.sleep(0.01)

        return ([a.json() for a in r] if len(r) > 1 else
                r[0].json()) if httpresp else self.process_response(r)

    def get_points(self, oid, params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "pts"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_notifications_modules(self, oid="", params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = "notmod"

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def get_notifications(self, module="", params=None, httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(module, str), 'Field <module> must be an module.'
        endpoint = "notmess"

        r = self.get(endpoint=endpoint, oid=module, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def consulta_miran_web(self, data=dict(), httpresp=False):
        # Checking data
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        # Checking consults data key
        assert 'consults' in data.keys(), 'Field <consults> does not exists.'
        assert isinstance(data['consults'], list), \
            'Field <consults> must be a list.'
        # Creating base route
        route = self.get_url() + self.get_endpoint("tags")
        # Requesting POST to API
        r = requests.post(route, 
                          json=data['consults'], 
                          headers=self.headers, 
                          proxies={'http': None, 'https': None})
        if httpresp:
            resp = r.json()
            resp['status_code'] = r.status_code
        return resp if httpresp \
            else self.process_response(r)

    def get_timeseries(self, oid="", params=dict(), httpresp=False):
        params = params or {}
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        endpoint = 'tssearch' if 'query' in params or 'name' in params else 'tsr'

        r = self.get(endpoint=endpoint, oid=oid, params=params)
        try:
            return r if httpresp else self.process_response(r)
        except Exception:
            return {
                "message": "There is an unknown problem.",
                "status": "error"
            } if httpresp else False

    def add_entity(self, data=dict(), force=False, httpresp=False):
        assert any(isinstance(data, i) for i in [dict, list]), \
            'Field <data> must be a dict or a list.'
        if isinstance(data, list):
            for d in data:
                assert isinstance(d, dict), 'The data element must be a dict.'
        if isinstance(data, dict):
            data = [data]
        assert isinstance(force, bool), 'Field <force> must be a str'

        r = self.post(endpoint="ent", data=data, force=force)
        if httpresp:
            resp = [a.json() for a in r]
            for i, s in zip(range(len(resp)), r):
                resp[i]['status_code'] = s.status_code

        return resp if httpresp \
            else self.process_response(r)

    def add_relationship(self, data=dict(), force=False, httpresp=False):
        assert any(isinstance(data, i) for i in [dict, list]), \
            'Field <data> must be a dict or a list.'
        if isinstance(data, list):
            for d in data:
                assert isinstance(d, dict), 'The data element must be a dict.'
        if isinstance(data, dict):
            data = [data]
        assert isinstance(force, bool), 'Field <force> must be a str'

        r = self.post(endpoint="rel", data=data, force=force)
        if httpresp:
            resp = [a.json() for a in r]
            for i, s in zip(range(len(resp)), r):
                resp[i]['status_code'] = s.status_code

        return resp if httpresp \
            else self.process_response(r)

    def add_timeseries(self, data=dict(), httpresp=False):
        assert any(isinstance(data, i) for i in [dict, list]), \
            'Field <data> must be a dict or a list.'
        if isinstance(data, list):
            for d in data:
                assert isinstance(d, dict), 'The data element must be a dict.'
        if isinstance(data, dict):
            data = [data]

        r = self.post(endpoint="tsr", data=data)
        if httpresp:
            resp = [a.json() for a in r]
            for i, s in zip(range(len(resp)), r):
                resp[i]['status_code'] = s.status_code

        return resp if httpresp \
            else self.process_response(r)

    def download_files(self, oids=[], pto=None, hashname=False, parallel=True):
        # Download files in parallel
        if parallel:
            # Use parallelism
            return Parallel(n_jobs=-1, backend="threading")(
                delayed(joblib_file_download)(
                    self, oid, pto, hashname) for oid in oids)
        else:
            # Does not use parallelism
            return [joblib_file_download(
                self, oid, pto, hashname) for oid in oids]
                

    def download_file(self, oid="", pto=None, hashname=False):
        return joblib_file_download(self, oid, pto, hashname)

    def link_file(self, data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        r = self.post(endpoint="linkfile", data=[data])
        if httpresp:
            resp = r[0].json()
            resp['status_code'] = r[0].status_code
        return resp if httpresp \
            else self.process_response(r[0])

    def unlink_file(self, data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        r = self.post(endpoint="unlinkfile", data=[data])
        if httpresp:
            resp = r[0].json()
            resp['status_code'] = r[0].status_code
        return resp if httpresp \
            else self.process_response(r[0])

    def upload_file(self, oid="", pfrom=None, pinfo=dict(),
                    session=None, options=dict(), httpresp=False):

        assert isinstance(oid, str), 'Field <oid> must be an oid.'
        assert isinstance(pfrom, str), 'Field <pfrom> must be a string.'
        assert isinstance(pinfo, dict), 'Field <pinfo> must be a dict.'
        assert isinstance(options, dict), 'Field <options> must be a dict.'

        # Capturing upload mode
        stream = options['stream'] if 'stream' in options else False
        # Capturing requests timeout
        timeout = options['timeout'] if 'timeout' in options else 300

        try:
            # Creating temporary directory
            dirpath = tempfile.mkdtemp()
            # If the url is a path for a file in the user file system
            if nt.isfile(pfrom):
                # Capturing the file name, full path and excluding url
                original_filename = pfrom.split("/")[-1].split('\\')[-1]
                filename = "upload." + original_filename.split(".")[-1]
                fullpath, pfrom = pfrom, ""
            elif "ftp://" in pfrom:
                # Capturing the file name
                original_filename = pfrom.split("/")[-1].split('\\')[-1]
                filename = "upload." + original_filename.split(".")[-1]
                # Generating full path
                fullpath = "%s/%s" % (dirpath, filename)
                # Writting file in temporary directory
                urlretrieve(pfrom, fullpath)
            else:
                # Capturing the contents of required url
                if session:
                    r = session.get(
                        pfrom, verify=False, stream=stream, timeout=timeout)
                else:
                    try:
                        r = requests.get(pfrom, verify=False, proxies={
                            'http': None, 'https': None
                        }, stream=stream, timeout=timeout)
                    except Exception:
                        # Resolvendo problema de validação do openssl
                        urllib3.disable_warnings()
                        urllib3.util.ssl_.DEFAULT_CIPHERS += ':HIGH:!DH:!aNULL'
                        r = requests.get(pfrom, verify=False, proxies={
                            'http': None, 'https': None
                        }, stream=stream, timeout=timeout)
                if not (200 <= r.status_code < 300):
                    raise Exception('')
                # Checking expected type from file
                if 'content_types' in options.keys():
                    if not any([i in r.headers.get(
                            'content-type') for i in options['content_types']]):
                        raise Exception('')
                # Capturing the file name
                original_filename = self.extract_filename(r)
                if 'symbolic_name' in options.keys():
                    original_filename = options['symbolic_name']
                filename = "upload." + original_filename.split(".")[-1]
                # Generating full path
                fullpath = "%s/%s" % (dirpath, filename)
                # Writting file in temporary directory
                with open(fullpath, 'wb') as f:
                    if stream:
                        try:
                            for chunk in r.iter_content(chunk_size=1024):
                                try:
                                    if chunk:
                                        f.write(chunk)
                                except Exception:
                                    continue
                        except Exception:
                            pass
                    else:
                        f.write(r.content)
            # Capturing filetype
            try:
                filetype = magic.from_file(fullpath, mime=True) or \
                    'application/octet-stream'
            except Exception:
                filetype = MimeTypes().guess_type(fullpath)[0] or \
                    'application/octet-stream'
            # Checking if the the file has a symbolic url
            if 'symbolic_url' in options.keys():
                pfrom = options['symbolic_url']
                del options['symbolic_url']
            # Checking if the the file has a symbolic name
            if 'symbolic_name' in options.keys():
                original_filename = options['symbolic_name']
            # Creating the file object
            fobj = {
                'url': pfrom,
                'obj_id': oid,
                'name': original_filename,
                'info': self.dumps(pinfo),
                'options': self.dumps(options),
                'file': (filename, open(fullpath, 'rb'), filetype)
            }
            # Sending data via post
            dburl = self.get_url() + self.get_endpoint("files")
            r = requests.post(dburl, files=fobj, headers={
                'Venidera-AuthToken': self.headers['Venidera-AuthToken']
            }, proxies={'http': None, 'https': None})
            # Removing temporary directory
            shutil.rmtree(dirpath)
            # Returning the response
            return r.json() if httpresp else self.process_response(r)
        except Exception:
            try:
                shutil.rmtree(dirpath)
            except Exception:
                pass

            return {
                "message": "Error on uploading the file.",
                "status": "error"
            } if httpresp else False

    def extract_filename(self, response):
        # Capturing filename
        filename = ""
        if 'Content-Disposition' in response.headers.keys():
            disp = response.headers['Content-Disposition']
            _, params = cgi.parse_header(disp)
            
            if 'filename' in params.keys():
                filename = params['filename']
            elif 'filename=' in disp:
                filename = disp.split('filename=')[1]
            elif "filename*=UTF-8''" in disp:
                filename = disp.split("filename*=UTF-8''")[1]
            if filename[0] == '"' or filename[0] == "'":
                filename = filename[1:-1]

        else:
            split = urlsplit(response.url)
            if split.fragment != "":
                return split.fragment
            filename = split.path.split("/")[-1].split('\\')[-1]
            if "#@" in filename:
                filename = filename.split("#@")[-1]
        return filename

    def add_points(self, oid="", data=dict(), httpresp=False):
        assert oid, 'Field <oid> must be a valid time series object.'
        assert any(isinstance(data, i) for i in [dict, list]), \
            'Field <data> must be a dict or a list.'
        if isinstance(data, list):
            for d in data:
                assert isinstance(d, dict), 'The data element must be a dict.'
        if isinstance(data, dict):
            data = [data]

        r = self.post(endpoint="pts", data=data, oid=oid)
        return ([a.json() for a in r] if len(r) > 1 else
                r[0].json()) if httpresp else self.process_response(r)

    def add_notifications_modules(self, name='', description='', location=None,
                                  httpresp=False):
        assert isinstance(name, str), \
            'Field <name> must be defined and have at least 3 characters.'
        assert isinstance(description, str), \
            'Field <description> must be defined.'
        assert isinstance(location, list) or location is None, \
            'Field <location> must be defined as a list with longitude and latitude only.'
        if len(name) < 3:
            raise AssertionError(
                'Field <name> must be defined and have at least 3 characters.')
        data = {'name': name, 'description': description}
        if location:
            data['location'] = location
        else:
            data['location'] = [-47.0878913, -22.893313]

        r = self.post(endpoint="notmod", data=[data])
        return ([a.json() for a in r] if len(r) > 1 else
                r[0].json()) if httpresp else self.process_response(r)

    def add_notification(self, module='', notification='', weight=1, objects_related='',
                         category='INFO', httpresp=False):
        if not objects_related:
            objects_related = []
        assert isinstance(module, str), \
            'Field <module> must be defined and have at least 3 characters.'
        assert isinstance(notification, str), \
            'Field <notification> must be defined.'
        assert isinstance(weight, (int, float)), \
            'Field <weight> must be int or float.'
        assert isinstance(objects_related, list), \
            'Field <objects_related> must be a list.'
        assert all([isinstance(x, str) for x in objects_related]), \
            'Field <objects_related> must be a list of strings.'
        assert category.upper() in ['ERROR', 'WARNING', 'INFO', 'info', 'FAIL'], \
            'Fied <category> must be one of these: [\'ERROR\', \'WARNING\', \'INFO\', \'info\', \'FAIL\']'
        category = category.upper()
        data = {'module': module}
        data['notification'] = notification
        data['weight'] = weight
        data['objects_related'] = objects_related
        data['category'] = category

        r = self.post(endpoint="notmess", data=[data])
        print(r)
        return ([a.json() for a in r] if len(r) > 1 else
                r[0].json()) if httpresp else self.process_response(r)

    def upd_text(self, oid="", data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert oid, 'Field <oid> must be a valid time series object.'

        r = self.put(endpoint="textupdate", data=data, oid=oid)
        return r.json() if httpresp else self.process_response(r)

    def upd_entity(self, oid="", data=dict(), params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert oid, 'Field <oid> must be a valid time series object.'

        r = self.put(endpoint="entupdate", data=data, oid=oid, params=params)
        return r.json() if httpresp else self.process_response(r)

    def upd_relationship(self, oid="", data=dict(), params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert oid, 'Field <oid> must be a valid time series object.'

        r = self.put(endpoint="relupdate", data=data, oid=oid, params=params)
        return r.json() if httpresp else self.process_response(r)

    def upd_timeseries(self, oid="", data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert oid, 'Field <oid> must be a valid time series object.'

        r = self.put(endpoint="tsrupdate", data=data, oid=oid)
        return r.json() if httpresp else self.process_response(r)

    def upd_file(self, oid="", data=dict(), httpresp=False):
        assert isinstance(data, dict), 'Field <data> must be a dict.'
        assert oid, 'Field <oid> must be a valid file object.'

        r = self.put(endpoint="fileupdate", data=data, oid=oid)
        return r.json() if httpresp else self.process_response(r)

    def del_entity(self, oid="", params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'

        r = self.delete(endpoint="ent", oid=oid, params=params)
        return r.json() if httpresp \
            else self.process_response(r, op='del')

    def del_relationship(self, oid="", params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'

        r = self.delete(endpoint="rel", oid=oid, params=params)
        return r.json() if httpresp \
            else self.process_response(r, op='del')

    def del_timeseries(self, oid="", params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'

        r = self.delete(endpoint="tsr", oid=oid, params=params)
        return r.json() if httpresp \
            else self.process_response(r, op='del')

    def del_file(self, oid="", params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'

        r = self.delete(endpoint="files", oid=oid, params=params)
        return r.json() if httpresp \
            else self.process_response(r, op='del')

    def del_points(self, oid="", params=dict(), httpresp=False):
        assert isinstance(params, dict), 'Field <params> must be a dict.'
        assert isinstance(oid, str), 'Field <oid> must be an oid.'

        r = self.delete(endpoint="pts", oid=oid, params=params)
        return r.json() if httpresp \
            else self.process_response(r, op='del')

    def dumps(self, x):
        return tdumps(x, default=self.encoding)

    def encoding(self, d):
        if isinstance(d, bytes):
            return d.decode('utf-8')
        elif isinstance(d, datetime):
            return d.isoformat()
        else:
            return str(d)
